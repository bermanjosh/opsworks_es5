#
# Cookbook:: opsworks_es5
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

elasticsearch_user 'elasticsearch' do
action :nothing
end

elasticsearch_install 'elasticsearch' do
type :package
version "5.0.2"
action :install
end
